//封装 请求
function post(url, param = {}, header = "application/json") {
  return new Promise((resolve, reject) => {
    wx.request({
      url: "http://127.0.0.1:8080"+url,
      method: "post",
      data:param,
      header: {
        'Content-Type': header,
        "token":wx.getStorageSync('token')
      },
      success: function (res) {
        var code = res.data.code
        if (code == 100000) {
          resolve(res.data)
        } else if (code == 100001) {
          wx.reLaunch({
            url: '/pages/login/login',
          })
        } else {
          wx.showToast({
            title: res.data.msg,
            duration: 2000
          })
        }
      },
      fail:function(res){
        reject(res)
      }
    })
  });
}

function get(url, param = {}, header = "application/json") {
  return new Promise((resolve, reject) => {
    wx.request({
      url: "http://127.0.0.1:8080"+url,
      method: "get",
      header: {
        'Content-Type': header
      },
      success: function (res) {
        console.log(res.data);
        var code = res.data.code
        if (code == 100000) {
          resolve(res.data)
        } else if (code == 100001) {
          wx.reLaunch({
            url: '/pages/login/login',
          })
        } else {
          wx.showToast({
            title: res.data.msg,
            duration: 2000
          })
        }
      },
      fail:function(res){
        reject(res)
      }
    })
  });
}

module.exports = {
  post:post,
  get:get
}