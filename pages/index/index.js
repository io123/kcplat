// index.js
// 获取应用实例
const app = getApp()
var http = require('../../utils/request.js')
Page({
  data: {
    signList: [],
    latitude:"",
    longitude:""
  },
  // 事件处理函数
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad() {
    wx.getLocation({
      type: 'wgs84',
      success: function (res) {
        that.setData({
          latitude: res.latitude,
          longitude: res.longitude
        })
      }
    })
    this.getSignList();
  },
  getSignList() {
    console.log("获取列表");
    var data = {
      "username": wx.getStorageSync('userId'),
      "userId": wx.getStorageSync('userId'),
      "dept_id": wx.getStorageSync('deptId'),
      "lat":this.data.latitude,
      "lon":this.data.longitude
    }
    var that = this;
    http.post("/api/signList", data).then(function (res) {
      console.log(res)
      var array = res.body;
      for(var i=0;i<array.length;i++){
         if(array[i].recordId==null){
          array[i].recordId="未签到"
         }else{
          array[i].recordId="已签到"
         }
      }
      that.setData({
        signList: res.body
      })
    })
  },
  signKc: function (params) {
    var sign = params.target.dataset.sign;
    console.log(sign)
    //获取地理位置放入其中
    var data = {
      "signId": sign.signId,
      "studentId": wx.getStorageSync('userId'),
      "lat": "0",
      "lon": "0"
    }
    var that = this;
    http.post("/api/signKc", data).then(function (res) {
      console.log(res)
      wx.showToast({
        title: "签到成功",
      })
      that.getSignList();
    })
  }
})