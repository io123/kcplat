var http = require('../../utils/request.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    username: '',
    password: ''
  },
  inputUserName: function (e) {
    console.log(e)
    this.setData({
      username: e.detail.value
    })
  },
  inputPassword: function (e) {
    console.log(e)
    this.setData({
      password: e.detail.value
    })
  },
  login: function () {
    console.log("登录系统")
    var data = {
      "username": this.data.username,
      "password": this.data.password,
      "userId": this.data.username
    }
    console.log(data)
    http.post("/api/login", data).then(function (res) {
     //保存登录凭证
     console.log(res)
      wx.setStorageSync('token', res.body.token);
      wx.setStorageSync('userId', res.body.userName);
      wx.setStorageSync('deptId', res.body.deptId);
      wx.reLaunch({
        url: '/pages/index/index',
      })
    });
  },
  register: function () {
    wx.navigateTo({
      url: '/pages/register/register',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})