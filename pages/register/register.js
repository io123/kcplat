// pages/register/register.js

var http= require('../../utils/request.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    deptList:[],
    IdArray:[],
    deptId:null,
    index: 0,
    userId:null,
    username:null,
    password:null,
    repassword:null
  },
  register:function(){
    if(this.data.deptId==null){
      wx.showToast({
        title: '请选择学院',
      })
      return
    }
    if(this.data.username==null){
      wx.showToast({
        title: '用户名不能为空',
      })
      return
    }
    if(this.data.password==null){
      wx.showToast({
        title: '密码不能为空',
      })
      return
    }
    if((this.data.password!=this.data.repassword)){
      wx.showToast({
        title: '两次密码不一致',
      })
      return
    }
    console.log("assa"+this.data.IdArray[this.data.index])
    var data={
      "userId":this.data.username,
      "username":this.data.username,
      "password":this.data.password,
      "register_type":"1",
      "dept_id":this.data.IdArray[this.data.index]
    }
    http.post("/api/register",data).then(function(res){
      console.log(res)
      if(res.code==100000){
        wx.setStorageSync('token', res.body.token);
        wx.setStorageSync('userId', res.body.userName);
        wx.setStorageSync('deptId', res.body.deptId);
        wx.reLaunch({
          url: '/pages/index/index',
        })
      }
    })
  },
  inputDeptName:function(e) {
     var value = e.detail.value;
     this.loadDeptList(value)
  },
  loadDeptList:function(v){
    var that = this
    http.get("/api/getDeptList?deptName="+v,{}).then(function(res){
      var deptName = new Array();
      var IdIndex = new Array();
      for(var i = 0;i<res.body.length;i++){
        deptName.push(res.body[i].dept_name);
        IdIndex.push(res.body[i].dept_id)
      }
     that.setData({
       deptList: deptName,
       IdArray:IdIndex
     })
    })
  },
  pickDept:function(e){
    console.log(e)
     var dept =  e.target.dataset.dept;
    this.setData({
      deptId:dept.dept_id
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    //默认加载
    this.loadDeptList(null);
  },
  inputUsername:function(e){
    this.setData({
       username:e.detail.value,
       userId:e.detail.value
    })
  },
  inputPassword:function(e){
    this.setData({
      password:e.detail.value
    })
  },
  repeatInputPassword:function(e){
    this.setData({
      repassword:e.detail.value
    })
  },
  bindPickerChange:function(e){
    console.log('picker下拉项发生变化后，下标为：', e.detail.value)
    this.setData({
        index: e.detail.value,
        deptId:this.data.IdArray[e.detail.value]
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})